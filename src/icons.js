import Vue from 'vue';

import { library } from '@fortawesome/fontawesome-svg-core';
// internal icons
import {
  faCheck,
  faArrowUp,
  faAngleRight,
  faAngleLeft,
  faChevronDown,
  faChevronUp,
  faImages,
  faTrophy,
  faExternalLinkAlt,
  faGamepad
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
    faCheck,
    faArrowUp,
    faAngleRight,
    faAngleLeft,
    faChevronDown,
    faChevronUp,
    faImages,
    faTrophy,
    faExternalLinkAlt,
    faGamepad
);
Vue.component('vue-fontawesome', FontAwesomeIcon);
