# iTalent winners
Статический вебсайт со всеми учасниками iTalent за 3-6 сезон.  
Лайв версия: [winners.italent.org.ua](https://winners.italent.org.ua)  
Дев: [italent-all-git-dev.aquaminer.now.sh](https://italent-all-git-dev.aquaminer.now.sh)

## Установка и сборка
```shell script
yarn install
yarn run develop 
# use "yarn run build" for build
```

## Что в пироге
Написано с использованием Vue.js фреймворка [Gridsome](https://gridsome.org), благодаря ему на выходе статичестикий сайт с множеством страничек в виде html файлов.
### Как это рабоатет
Что делает Gridsome описано на [главной странице](https://gridsome.org) фреймворка.   
Если описывать кратко. Все данные собираются в _gridosme.server.js_, часть из них берётся из csv и json файлов которые можно найти в _data_. Другая часть(3 и 6 сезон) берётся из гугл таблиц, испольузется модфиицированая либа [google-spreadsheet-lite](https://github.com/aquaminer/node-google-spreadsheet).
### Что в итоге
В итоге шикарный оптимизированый многостраничник, спокойно набирает 90+ баллов в Lighthouse. За счёт пререндеринга работает даже на IE. 
