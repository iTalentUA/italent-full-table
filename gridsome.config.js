// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'iTalent',
  siteDescription: 'Веб сайт з роботами учасників iTalent за 3,4,5,6 сезон.',
  siteUrl: 'https://winners.italent.org.ua',
  metadata: {
    meta: [
      { property: 'og:locale', content: 'uk' },
      { property: 'theme-color', content: '#00b1ff'}
    ],
  },
  // outputDir: "public",
  plugins: [
    {
      use: '@gridsome/plugin-google-analytics',
      options: {
        id: 'UA-107920964-1'
      }
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        cacheTime: 600000, // default
        config: {
          '/*': {
            changefreq: 'weekly',
            priority: 0.5
          },
          '/cybersport': {
            changefreq: 'monthly',
            priority: 0.5
          }
        }
      }
    },
    {
      use: "gridsome-plugin-manifest",
      options: {
        background_color: "#FFFFFF",
        icon_path: "./src/assets/img/appicon.png",
        name: "Переможці iTalent",
        short_name: "iTalent",
        theme_color: "#00b1ff",
        lang: "uk",
      },
    },
    {
      use: "gridsome-plugin-service-worker",
      options: {
        networkFirst: {
          cacheName: "nf-v1",
          routes: ["/", /\.(js|css|png)/],
        }
      }
    },
    {
      use: "gridsome-plugin-svg",
      options: {
        goesBothWays: true
      }
    }
  ],
  templates: {
    NewNomination: '/season7/:slug',
    Nomination: '/season:season/:path',
    Season: '/season:id',
  }
};
