// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import './assets/scss/app.scss';
import './icons.js';
import Buefy from './buefy';
import Vue from 'vue';
import SimpleAnalytics from "simple-analytics-vue";

import { isMobile } from 'buefy/src/utils/helpers.js';
Vue.prototype.$isMobile = () => {
  return isMobile.any() !== null;
}


import DefaultLayout from '~/layouts/Default.vue'

export default function (Vue, { router, head, isClient }) {
  head.htmlAttrs = { lang: 'uk' };

  Vue.use(Buefy, {
    defaultIconComponent: 'vue-fontawesome',
    defaultIconPack: 'fas',
  });

  try {
    Vue.use(require('vue-waypoint').default);
  }catch (e) {
    e;
  }
  if(typeof window !== 'undefined') {
    router.beforeEach((to, from, next) => {
      window.loading = Vue.prototype.$buefy.loading.open();
      next()
    });
    router.afterEach(() => {
      window.loading.close();
    });
    Vue.use(SimpleAnalytics, { skip: process.env.NODE_ENV !== "production", domain: 'italent.stats.aquaminer.me' })
  }
  Vue.component('Layout', DefaultLayout)
}
