// from: https://github.com/buefy/buefy/blob/dev/src/index.js
/* eslint-disable */
import {
   Modal, Icon, Table,
   Button, Notification, Switch, Navbar, Dropdown, Checkbox, Loading
} from 'buefy/src/components';
//
import { merge } from 'buefy/src/utils/helpers';
import config, { setOptions } from 'buefy/src/utils/config';
import { use, registerComponentProgrammatic } from 'buefy/src/utils/plugins';

const components = {
  Modal, Icon, Table, Button, Notification, Switch, Navbar, Dropdown, Checkbox, Loading
};

const Buefy = {
  install(Vue, options = {}) {
    // Options
    setOptions(merge(config, options, true));
    config.defaultIconComponent = options.defaultIconComponent;
    config.defaultIconPack = options.defaultIconPack;
    // Components

    // eslint-disable-next-line guard-for-in
    for (const componentKey in components) {
      Vue.use(components[componentKey]);
    }
    // Config component
    const BuefyProgrammatic = {
      // eslint-disable-next-line no-shadow
      setOptions(options) {
        setOptions(merge(config, options, true));
      },
    };
    registerComponentProgrammatic(Vue, 'config', BuefyProgrammatic);
  },
};


export default Buefy;

use(Buefy);

// export all components as vue plugin
export {
  Modal, Icon, Table, Button, Notification, Navbar, Dropdown, Checkbox, Loading
};
// export programmatic component
// export { DialogProgrammatic } from 'buefy/src/components/dialog';
export { LoadingProgrammatic } from 'buefy/src/components/loading';
// export { ModalProgrammatic } from 'buefy/src/components/modal';
// export { NotificationProgrammatic } from 'buefy/src/components/notification';
// export { SnackbarProgrammatic } from 'buefy/src/components/snackbar';
// export { ToastProgrammatic } from 'buefy/src//components/toast';
