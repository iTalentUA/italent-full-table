// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const fs = require('fs');

const Papa = require('papaparse');
const GoogleSpreadsheet = require('google-spreadsheet-lite');
const { deserialize, serialize } = require('v8');
const axios = require('axios');

module.exports = function (api) {
    api.loadSource(async actions => {
        actions.addSchemaTypes(`
            type Season implements Node {
              desc: String
              photos_url: String
            }

            type Nomination implements Node {
              title: String
              path: String
              header: [String!]
              season: ID
              haveCategory: Boolean
              haveWrong: Boolean
            }
            
            type Project implements Node {
              title: String
              name: String
              age: Int
              score: Int
              # category: String
              type: String
              isWin: Boolean
              estimated: Boolean
              highCategory: Boolean
              nominationId: ID!
              row: [String!]
            }
            
            type CyberCommand {
              title: String,
              squad: [String!],
              place: Int,
              game: String
            }
            
            type CyberSeason implements Node {
              season: Int,
              games: [String!],
              commands: [CyberCommand!]
            }
            
            type WinnerPerson {
              name: String!
              age: Int!
            }
            
            type WinnerProject implements Node {
              place: Int
              season: ID!
              nomination_title: String
              school: String
              highCategory: Boolean
              persons: [WinnerPerson!]
              link: String
              image: Image
              preview_type: String
            }
          `);

        let seasons = actions.addCollection('Season');
        let nominations = actions.addCollection('Nomination');
        let projects = actions.addCollection('Project');
        let cyber_nominations = actions.addCollection('CyberSeason');

        const seasons_data = JSON.parse(fs.readFileSync('./data/seasons.json'));
        seasons_data.forEach((val) => {
            seasons.addNode(val);
        });

        const nominations_data = {
            2: '1rWZ5uewe7KX135lsJQY1KuCybiu1aad7_OlJpSqghXg',
            3: '1J8q-LgNeZMrjV8Wi1Pn7y27i-tVADVWwybHJL2__g_0',
            4: [
                {name: "2Д ГРАФІКА"},
                {name: "3Д ГРАФІКА"},
                {name: "2Д АНІМАЦІЯ"},
                {name: "3Д АНІМАЦІЯ"},
                {name: "SCRATCH", winers: 5},
                {name: "АПАРАТНО-ПРОГРАМНА РОЗРОБКА"},
                {name: "ВЕБ APPLICATION"},
                {name: "ВЕБ FRONT-END"},
                {name: "ВЕБ ШКІЛЬНИЙ САЙТ"},
                {name: "ВІДЕОМОНТАЖ"},
                {name: "ГРА"},
                {name: "ПРОГРАМНА РОЗРОБКА", "winers": [2 , 3]},
                {name: "ФОТОГРАФІЯ"}
            ],
            5: [
                {name: "2Д АНІМАЦІЯ"},
                {name: "2Д ГРАФІКА"},
                {name: "3Д ГРАФІКА"},
                {name: "SCRATCH"},
                {name: "SOFT&MOBILE"},
                {name: "ВЕБ APPLICATION"},
                {name: "ВЕБ FRONT-END"},
                {name: "ВЕБ ШКІЛЬНИЙ САЙТ"},
                {name: "Розробка девайсів"},
                {name: "ГРА"},
                {name: "ВІДЕОМОНТАЖ"},
                {name: "ФОТОГРАФІЯ"}
            ],
            6: '13X0bQ6yIIgsMTRNVP2oMEN0ke8EPnlcD6xtz7qb_3KM'
        };

        const sheets_data_raw = fs.readFileSync('./data/sheets.bin');
        const sheets_data = sheets_data_raw.length ? deserialize(sheets_data_raw) : {};

        async function getEndData(sheet) {
            const id = sheet.url;

            
            if(sheets_data[id]) return sheets_data[id];

            let cells = await sheet.getCells({
                'return-empty': false
            });

            let end_data = [];

            cells.forEach((cell) => {
                if(end_data[cell.row] === undefined){
                    end_data[cell.row] = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
                }
                end_data[cell.row][cell.col] = cell._numericValue === undefined ? cell._value : cell._numericValue;
            });

            sheets_data[id] = end_data;
            return end_data;
        }

        var procData = (data, nominationId, nomination_name, season) => {
            /** @type {array} */
            let header = null;
            let drop_first = 0;

            let name_index = null;
            let age_index = null;
            let title_index = null;
            let score_index = null;

            let type = null;
            let category = null;
            let have_wrong = false;
            for(index in data){
                /** @type {array} */
                let row = data[index];

                if(header === null){
                    for (let i = 0; i < 5; i++) {
                        if(row[i] === 'Вік') header = row;
                    }
                }

                if(row === header) {  //заголовке (header) есть все необходимые столбцы, однако в 3-м сзеоне встречается пустатоа, теость первые две пустые, это рботате не для всех таблиц поэетому счётчик пустых тут. Далее просто игнорим эти начальные.
                    for (let i = 0; i < 3; i++) {
                        if (row[i] === '') drop_first++;
                    }
                    header = row;
                }
                for (let i = 0; i < drop_first; i++) {
                    row.shift();
                }
                if(row === header) {
                    for (let i = 0; i < 10; i++) {
                        if(row[i] === 'ФІО' || row[i] === 'ПІБ') name_index = i;
                        else if (row[i] === 'Вік') age_index = i;
                        else if (row[i] === 'Назва роботи') title_index = i;
                        else if (row[i] === 'Середня оцінка') score_index = i;
                        // всеголишь 4-е постоянных поля, остальные часто варируются
                    }
                }
                const fail = "РОБОТИ ЩО НЕ БУЛИ ОЦІНЕНІ ЗА ТЕХНІЧНИМИ ПРИЧИНАМИ";
                if(row[title_index] === ''){
                    if(row[name_index] === "МОЛОДША ВІКОВА ГРУПА" || row[name_index] === "СТАРША ВІКОВА ГРУПА"){
                        category = row[name_index];
                    }else if(row[name_index] === "ФІНАЛІСТИ" || row[name_index] === "КРАЩІ РОБОТИ" || row[name_index] === fail){
                        type = row[name_index];
                    }

                }else if(row[title_index] !== '' && row[title_index] !== undefined && row !== header){
                    if(have_wrong === false && type === fail) have_wrong = true;
                    projects.addNode({
                        id: row[name_index] + " - " + row[title_index] + Math.random(),
                        title: row[title_index],
                        name: row[name_index],
                        age: row[age_index],
                        score: row[score_index],
                        type: type,
                        isWin: type === "ФІНАЛІСТИ",
                        estimated: type !== fail,
                        highCategory: category === "СТАРША ВІКОВА ГРУПА",
                        // category: category,
                        row: row,
                        nominationId: nominationId
                    })
                }
            }

            nominations.addNode({
                id: nominationId,
                title: nomination_name,
                path: rus_to_latin(nomination_name),
                header: header,
                haveCategory: category !== null,
                haveWrong: have_wrong,
                season: season.toString()
            });
            have_wrong = false;
            category = null;
        };


        for (let season in nominations_data) {
            /** @type {array} */
            const season_data = nominations_data[season];
            if(typeof season_data === 'string'){
                var doc = new GoogleSpreadsheet(season_data);

                let info = await doc.getInfo();
                console.log('Loaded doc: '+info.title+' by '+info.author.email);
                for(let sheet_key in info.worksheets){
                    let sheet = info.worksheets[sheet_key];
                    console.log('sheet: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);

                    procData(await getEndData(sheet), season.toString() + sheet.title, sheet.title, season);
                }
                
                fs.writeFileSync('./data/sheets.bin', serialize(sheets_data));
            } else {
                season_data.forEach((nomination, nomination_index) => {
                    const file = `./data/${season}/${nomination.name}.csv`;
                    const nominationId = season.toString() + nomination_index.toString();
                    fs.readFile(file, (err, data) => {
                        if (err) throw err;

                        var {data} = Papa.parse(data.toString());
                        procData(data, nominationId, nomination.name, season);
                    });
                });
            }
        }

        const cyber_data = JSON.parse(fs.readFileSync('./data/cybersport.json'));
        for (let season in cyber_data) {
            var commands = [];
            var games = [];
            for (let game in cyber_data[season]){
                let lcommands = cyber_data[season][game];
                lcommands.forEach((val, key) => {
                    val.game = game;
                    commands.push(val);
                });

                games.push(game);
            }
            cyber_nominations.addNode({
                season: season,
                games: games,
                commands: commands
            });
        }
        console.log('get finalsit...')

        let winner_projects = actions.addCollection('WinnerProject');
        let finalist_doc = new GoogleSpreadsheet('1zQMwcuDueqE32n-7-hYUVQaFc0Fu38NTpIsxhzACK0w');

        const sheet_data = await getEndData((await finalist_doc.getInfo()).worksheets.shift());

        sheet_data.forEach((row, index) => {
            if(index <= 1) return; //skip header

            row.shift(); //empty row
        
            const season = row.shift().toString();
            const place = row.shift();
            const nomination_title = row.shift();
            const highCategory = row.shift() === "М";

            let persons = [];
            const name = row.shift();
            let splited = name.split(', ');
            splited.forEach((name, index) => {
                persons[index] = {
                    name
                };
            });

            const age = row.shift();
            if(typeof age === 'string'){
                splited = age.split(' / ');
                splited.forEach((age, index) => {
                    persons[index].age = Number(age);
                });
            }else{
                persons[0].age = age;
            }

            const school = row.shift();
            const link = row.shift();
            const preview_type = row.shift();
            winner_projects.addNode(Object.assign({
                season,
                place,
                nomination_title,
                highCategory,
                persons,
                school,
                preview_type
            },
                (link.charAt(0) === '/' ?
                    {
                        image: __dirname + link
                    }
                    :
                    {
                        link
                    }
            )))
        })
        actions.addSchemaTypes(`
        type AgeGradeFull {
            id: ID!
            toAge: Int
            fromAge: Int

        }
        type Criterion {
            id: ID!
            votable: Boolean
            title: String
            description: String
            iconUrl: String
        }
        type NewNomination implements Node {
            id: String!
            title: String!
            slug: String!
            isTech: Boolean!
            isNew: Boolean!
            available: Boolean!
            ageGrades: [AgeGradeFull!]
            votableCriterions: [Criterion]
          }
        type User {
          id: ID!
          firstName: String
          lastName: String
          age: Int
        }
        
        type AgeGrade {
          id: Int!
          nominationId: ID!
        }
        
        type Comment {
          id: Int!
          senderId: Int!
          text: String!
          sender: User!
        }
        
        type Command {
          title: String
          authors: [User]
        }
        type NewProject implements Node {
            id: ID!
            title: String!
            status: Int!
            ownerId: Boolean!
            avgVote: Int!
            place: Int
            sumVote: Float
            owner: User
            nominationId: String!
            ageGrade: AgeGrade
            comments:[Comment]
            command: Command
            votesPerCriterion: JSON
            previewFileId: String
            projectFileId: String
            video: String
            siteLink: String
            sourceLink: String
            procVideo: String
            pressVideo: String
            gameVideo: String
        }
          `)
          let new_nominations = actions.addCollection('NewNomination');

          const httpClient = axios.create({
            baseURL: 'https://reg.italent.org.ua/api/v0/'
          })

        console.log('start fetching nominations');
          const nominationRows =  (await httpClient.get('nomination/get')).data;

          for (const row of nominationRows) {
              const nominationFull = (await httpClient.get(`nomination/${row.id}/getFull`)).data;
              row.ageGrades = nominationFull.ageGrades;
              new_nominations.addNode({
                  ...row,
                  votableCriterions: (await httpClient.get(`nomination/${row.id}/votableCriterions`)).data.votableCriterions
              });
          }
        console.log('end fetching nominations');

        console.log('start fetching projects');

        let new_projects = actions.addCollection('NewProject');
        const projectRows = (await httpClient.get('project/allForEnd')).data.rows;

        for (const row of projectRows) {
            row.sumVote = Number(row.sumVote).toFixed(2)

            new_projects.addNode({
                ...row,
                nominationId: String(row.ageGrade.nominationId)
            });
        }

        console.log('end fetching projects');
    })
};


function rus_to_latin ( str ) { //auto converter to lower case, org: https://gist.github.com/diolavr/d2d50686cb5a472f5696
    // noinspection NonAsciiCharacters
    var ru = {
        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd',
        'е': 'e', 'ё': 'e', 'ж': 'j', 'з': 'z', 'и': 'i', 'і': 'i',
        'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
        'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u',
        'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
        'щ': 'shch', 'ы': 'y', 'э': 'e', 'ю': 'u', 'я': 'ya'
    }, n_str = [];

    str = str.replace(/[ъь]+/g, '').replace(/й/g, 'i');

    for ( var i = 0; i < str.length; ++i ) {
        n_str.push(
            (ru[ str[i] ]
                || ru[ str[i].toLowerCase() ] == undefined && str[i]
                || ru[ str[i].toLowerCase() ].replace(/^(.)/, function ( match ) { return match.toUpperCase() })).toLowerCase()
        );
    }

    return n_str.join('');
}
